{-# LANGUAGE NoImplicitPrelude #-}
{-# LANGUAGE OverloadedStrings #-}

import Relude.Applicative ()
import Relude.Base (IO)
import Relude.Bool ()
import Relude.Container ()
import Relude.Debug ()
import Relude.DeepSeq ()
import Relude.Exception ()
import Relude.File ()
import Relude.Foldable ()
import Relude.Function ()
import Relude.Functor ()
import Relude.Lifted (putStrLn)
import Relude.List (
    (++),
    head,
    viaNonEmpty)
import Relude.Monad (Maybe(..))
import Relude.Monoid ()
import Relude.Nub ()
import Relude.Numeric ()
import Relude.Print ()
import Relude.String ()
import qualified Relude.Unsafe as Unsafe ( (!!) )
import Relude ()

import System.Environment (getArgs)

main :: IO ()
main = do
    args <- getArgs
    case viaNonEmpty head args of
        Nothing -> putStrLn "This should not happens."
        Just s -> putStrLn ("You called: " ++ s)
    putStrLn ("Hello, " ++ args Unsafe.!! 1)
