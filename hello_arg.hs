{-# LANGUAGE NoImplicitPrelude #-}
{-# LANGUAGE OverloadedStrings #-}

import Relude.Applicative ()
import Relude.Base (IO)
import Relude.Bool ()
import Relude.Container ()
import Relude.Debug ()
import Relude.DeepSeq ()
import Relude.Exception ()
import Relude.File ()
import Relude.Foldable ()
import Relude.Function ()
import Relude.Functor ()
import Relude.Lifted (putStrLn)
import Relude.List (
    (++),
    head,
    tail,
    viaNonEmpty)
import Relude.Monad (Maybe(..))
import Relude.Monoid ()
import Relude.Nub ()
import Relude.Numeric ()
import Relude.Print ()
import Relude.String (fromString)
import qualified Relude.Unsafe as Unsafe ( (!!) )
import Relude ()

import System.Environment (getArgs)

main :: IO ()
main = do
    args <- getArgs
    case viaNonEmpty head args of
        Nothing -> putStrLn "This should not happens."
        Just _ -> case viaNonEmpty tail args of
            Nothing -> putStrLn "Usage: first arg is used, others are ignored"
            Just t -> case viaNonEmpty head t of
                Nothing -> putStrLn "One"
                Just s -> putStrLn ("Hello, " ++ s)

    -- https://www.idris-lang.org/docs/1.0/prelude_doc/docs/Prelude.List.html
