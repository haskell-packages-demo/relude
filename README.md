# relude

S[R](https://repology.org/project/haskell:relude/versions)ArNi Safe, performant, user-friendly and lightweight Haskell Standard Library (total programming) [relude](http://hackage.haskell.org/package/relude)

* [*No implicit Prelude*
  ](https://typeclasses.com/ghc/no-implicit-prelude)
